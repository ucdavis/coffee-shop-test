Coding Challenge: Cafe Point-of-Sale
====================================

Challenge
---------
Write a simple cafe "point-of-sale" (POS) system.

You may use any programming language you wish, e.g. Javascript, Python, Ruby, Java, C#, etc.

You are provided two JSON files:
 * menu.json, containing all items on the coffee shop's menu and
 * orders.json, containing commands ('orders') to be given to the system such as
   'addItem' or 'dueAmount'.

You are also provided a sample solution in output.txt.

You may _not_ use the Internet to look up the answers to this test but you _can_ use the Internet for regular
programming queries, e.g. how do I use map() in my programming language? (Just like you would at work.)

Specifications
--------------

1) The program you write must take two command line arguments: a menu file and an orders file, e.g.

    my-cafe-program menu.json orders.json

where menu.json and orders.json are the two given files.

2) Using the data in the menu, your program must parse the cafe orders found in orders.json and
output text to the console according to the following specificaitons:

NOTE: Orders are fulfilled in a FIFO (first-in, first-out) order.

'addOrder'     : Adds the name of the item to the end of the orders array if it exists on the menu and prints "Order added!", otherwise print "This item is currently unavailable!"
'listOrders'   : Prints unfulfilled orders in the order they were received separated by a comma and space. If no orders, prints "No orders to list!"
'dueAmount'    : Prints the amount due to two decimal places. If no orders, returns 0.00
'fulfillOrder' : If the orders array is not empty, print "The {item} is ready!". If the orders array is empty, print "All orders have been fulfilled!". See note above about item ordering.
'cheapestItem' : Prints the name of the cheapest item on the menu.
'drinksOnly'   : Prints only the item names of type drink from the menu separated by a comma and a space.
'foodOnly'     : Prints only the item names of type food from the menu separated by a comma and a space.

Evaluation
----------
You will be evaluated on:

* Correctness of the output (a simple 'diff' between your output and output.txt should reveal no differences.)
* Cleanliness of code
* Readability of code
* General programming competencies, e.g. clean architecture, DRY code, etc.

If you cannot finish the allotted time please still turn in a response. Incomplete responses may still be evaluated.
